/* rho/sparse_set.h - v0.3.0 - MIT - Oleksandr Manenko, May 2021
 * A header-only library that implements sparse set data structure.
 *
 *******************************************************************************
 * USAGE
 *
 * This file provides both the interface and implementation.
 *
 * Copy the "rho" directory into your source tree and then in exactly one source
 * file:
 *     #define RHO_SPARSE_SET_IMPLEMENTATION
 *     #include "rho/sparse_set.h"
 * This will create the implementation.
 * Other source files should just include "rho/sparse_set.h".
 *
 * Optionally provide one of the following defines with your implementation:
 *
 * RHO_SS_CUSTOM_ID    - Define it to provide custom rho_ss_id_t and
 *                       rho_ss_id_max.
 *                       The (rho_ss_id_max + 1) should not overflow.
 *                       If the macro is not defined, the library defines
 *                       rho_ss_id_t as uint16_t and
 *                       rho_ss_id_max as UINT16_MAX - 1.
 * RHO_SS_ASSERT(COND) - Your own assert macro (default: assert(COND)).
 * RHO_SS_MALLOC(SIZE) - Your own malloc function (default: malloc(SIZE)).
 * RHO_SS_FREE(PTR)    - Your own free function (default: free(PTR)).
 * RHO_SS_API_DECL     - Public function declaration prefix (default: extern).
 * RHO_SS_API_IMPL     - Public function implementation prefix (default is
 *                       empty). You can define it as static to make
 *                       implementation private to the source file that creates
 *                       it. This way you can have multiple implementations in
 *                       your project.
 *
 *******************************************************************************
 * LICENSE
 *
 * Scroll to the end of the file for license information.
 */

#ifndef RHO_SPARSE_SET_H_INCLUDED
#    define RHO_SPARSE_SET_H_INCLUDED ( 1 )

#    include <stdbool.h>
#    include <stddef.h>

#    ifdef __cplusplus
extern "C" {
#    endif

/* Interface ******************************************************************/

#    ifndef RHO_SS_API_DECL
#        ifdef RHO_API_DECL
#            define RHO_SS_API_DECL RHO_API_DECL
#        else
#            define RHO_SS_API_DECL extern
#        endif
#    endif

#    ifndef RHO_SS_CUSTOM_ID
#    include <stdint.h>
/** An item stored in a sparse set. */
typedef uint16_t rho_ss_id_t;
/** Maximum possible value of the rho_ss_id_t. */
static const rho_ss_id_t rho_ss_id_max = UINT16_MAX - 1;
#    endif

/** The sparse set. */
typedef struct {
    rho_ss_id_t *sparse; /**< Sparse array used to speed-optimise the set.    */
    rho_ss_id_t *dense;  /**< Dense array that stores the set's items.        */
    rho_ss_id_t  n;      /**< Number of items in the dense array.             */
    rho_ss_id_t  max;    /**< Maximum allowed item ID.                        */
} rho_ss_t;

/**
 * Allocate the sparse set that can hold IDs up to max_id.
 *
 * The set can contain up to (max_id + 1) items.
 */
RHO_SS_API_DECL rho_ss_t rho_ss_alloc( rho_ss_id_t max_id );

/**
 * Free the sparse set resources, previously allocated with rho_ss_alloc.
 */
RHO_SS_API_DECL void rho_ss_free( rho_ss_t *ss );

/**
 * Remove all items from the sparse set but do not free memory used by the set.
 */
RHO_SS_API_DECL void rho_ss_clear( rho_ss_t *ss );

/**
 * Return the number of items in the sparse set.
 */
RHO_SS_API_DECL size_t rho_ss_size( rho_ss_t *ss );

/**
 * Return a value indicating whether the sparse set is full and cannot add new
 * items.
 */
RHO_SS_API_DECL bool rho_ss_full( rho_ss_t *ss );

/**
 * Return a value indicating whether the sparse set has no items.
 */
RHO_SS_API_DECL bool rho_ss_empty( rho_ss_t *ss );

/**
 * Return a value indicating whether the sparse set contains the given item.
 */
RHO_SS_API_DECL bool rho_ss_has( rho_ss_t *ss, rho_ss_id_t id );

/**
 * Add an item to the sparse set unless it is already there.
 */
RHO_SS_API_DECL void rho_ss_add( rho_ss_t *ss, rho_ss_id_t id );

/**
 * Remove an item from the sparse set unless it is not there.
 */
RHO_SS_API_DECL void rho_ss_remove( rho_ss_t *ss, rho_ss_id_t id );

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_SPARSE_SET_H_INCLUDED */

#ifdef RHO_SPARSE_SET_IMPLEMENTATION
#    ifdef __cplusplus
extern "C" {
#    endif

/* Implementation *************************************************************/

#    ifndef RHO_SS_API_IMPL
#        ifdef RHO_API_IMPL
#            define RHO_SS_API_IMPL RHO_API_IMPL
#        else
#            define RHO_SS_API_IMPL
#        endif
#    endif

#    ifndef RHO_SS_ASSERT
#        ifdef RHO_ASSERT
#            define RHO_SS_ASSERT( COND ) RHO_ASSERT( COND )
#        else
#            include <assert.h>
#            define RHO_SS_ASSERT( COND ) assert( COND )
#        endif
#    endif

#    ifndef RHO_SS_MALLOC
#        ifdef RHO_MALLOC
#            define RHO_SS_MALLOC( SIZE ) RHO_MALLOC( SIZE )
#        else
#            include <stdlib.h>
#            define RHO_SS_MALLOC( SIZE ) malloc( SIZE )
#        endif
#    endif

#    ifndef RHO_SS_FREE
#        ifdef RHO_FREE
#            define RHO_SS_FREE( PTR ) RHO_FREE( PTR )
#        else
#            include <stdlib.h>
#            define RHO_SS_FREE( PTR ) free( PTR )
#        endif
#    endif

RHO_SS_API_IMPL rho_ss_t rho_ss_alloc( rho_ss_id_t max_id )
{
    RHO_SS_ASSERT( max_id > 0 && max_id <= rho_ss_id_max );

    size_t   array_size = sizeof( rho_ss_id_t ) * ( max_id + 1 );
    rho_ss_t ss         = { 0 };

    ss.dense  = RHO_SS_MALLOC( array_size );
    ss.sparse = RHO_SS_MALLOC( array_size );

    if ( !ss.dense || !ss.sparse ) {
        RHO_SS_FREE( ss.dense );
        RHO_SS_FREE( ss.sparse );

        ss.dense  = NULL;
        ss.sparse = NULL;
    } else {
        ss.max = max_id;
    }

    return ss;
}

RHO_SS_API_IMPL void rho_ss_free( rho_ss_t *ss )
{
    RHO_SS_ASSERT( ss );

    RHO_SS_FREE( ss->dense );
    RHO_SS_FREE( ss->sparse );

    ss->dense  = NULL;
    ss->sparse = NULL;
    ss->max    = 0;
    ss->n      = 0;
}

RHO_SS_API_IMPL void rho_ss_clear( rho_ss_t *ss )
{
    RHO_SS_ASSERT( ss );

    ss->n = 0;
}

RHO_SS_API_IMPL size_t rho_ss_size( rho_ss_t *ss )
{
    RHO_SS_ASSERT( ss );

    return ss->n;
}

RHO_SS_API_IMPL bool rho_ss_full( rho_ss_t *ss )
{
    RHO_SS_ASSERT( ss );

    return ss->n > ss->max;
}

RHO_SS_API_IMPL bool rho_ss_empty( rho_ss_t *ss )
{
    RHO_SS_ASSERT( ss );

    return ss->n == 0;
}

RHO_SS_API_IMPL bool rho_ss_has( rho_ss_t *ss, rho_ss_id_t id )
{
    RHO_SS_ASSERT( ss );
    RHO_SS_ASSERT( id <= ss->max );

    rho_ss_id_t dense_index = ss->sparse[id];

    return dense_index < ss->n && ss->dense[dense_index] == id;
}

RHO_SS_API_IMPL void rho_ss_add( rho_ss_t *ss, rho_ss_id_t id )
{
    RHO_SS_ASSERT( ss );
    RHO_SS_ASSERT( id <= ss->max );

    if ( rho_ss_has( ss, id ) ) {
        return;
    }

    ss->dense[ss->n] = id;
    ss->sparse[id]   = ss->n;

    ++ss->n;
}

RHO_SS_API_IMPL void rho_ss_remove( rho_ss_t *ss, rho_ss_id_t id )
{
    RHO_SS_ASSERT( ss );
    RHO_SS_ASSERT( id <= ss->max );

    if ( rho_ss_has( ss, id ) ) {
        --ss->n;
        rho_ss_id_t dense_index = ss->sparse[id];
        rho_ss_id_t item        = ss->dense[ss->n];
        ss->dense[dense_index]  = item;
        ss->sparse[item]        = dense_index;
    }
}

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_SPARSE_SET_IMPLEMENTATION */

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
