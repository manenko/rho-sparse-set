#include <stdint.h>

#define RHO_SS_CUSTOM_ID ( 1 )
#define RHO_SPARSE_SET_IMPLEMENTATION

typedef uint32_t         rho_ss_id_t;
static const rho_ss_id_t rho_ss_id_max = UINT32_MAX - 1;

#include <rho/sparse_set.h>
#include <rho/test.h>

int main( void )
{
    static const rho_ss_id_t max_id = 256;

    rho_ss_t ss = rho_ss_alloc( max_id );
    rho_test(
        "when sparse set is created, its dense array is allocated",
        ss.dense != NULL );
    rho_test(
        "when sparse set is created, its sparse array is allocated",
        ss.sparse != NULL );
    rho_test(
        "when sparse set is created, its next item's index is zero",
        ss.n == 0 );
    rho_test(
        "when sparse set is created, its capacity is the same as the maximum "
        "id provided during the creation",
        ss.max == max_id );
    rho_test(
        "when sparse set is created, it is not full",
        !rho_ss_full( &ss ) );
    rho_test( "when sparse set is created, it is empty", rho_ss_empty( &ss ) );

    rho_ss_add( &ss, 0 );
    rho_test(
        "when an item is added to the sparse set, it is empty no more",
        !rho_ss_empty( &ss ) );
    rho_test(
        "when an item is added to the sparse set, the set contains the item",
        rho_ss_has( &ss, 0 ) );

    rho_ss_add( &ss, 256 );
    rho_test(
        "when an item is added to the sparse set, it is not empty",
        !rho_ss_empty( &ss ) );
    rho_test(
        "when an item is added to the sparse set, it contains the previously "
        "added item",
        rho_ss_has( &ss, 0 ) );
    rho_test(
        "when an item is added to the sparse set, it contains the newly added "
        "item",
        rho_ss_has( &ss, 256 ) );

    rho_ss_clear( &ss );
    rho_test( "when cleared the sparse set, it is empty", rho_ss_empty( &ss ) );

    rho_ss_add( &ss, 256 );
    rho_ss_add( &ss, 256 );
    rho_ss_add( &ss, 14 );
    rho_ss_add( &ss, 127 );
    rho_ss_add( &ss, 5 );
    rho_test(
        "when added a few items to the sparse set, it has correct size",
        rho_ss_size( &ss ) == 4 );
    rho_test(
        "when added a few items to the sparse set, it contains all new items",
        rho_ss_has( &ss, 14 ) && rho_ss_has( &ss, 256 )
            && rho_ss_has( &ss, 127 ) && rho_ss_has( &ss, 5 ) );

    rho_ss_remove( &ss, 0 );
    rho_test(
        "when removed non-existed item from the sparse set, the set's size has "
        "not changed",
        rho_ss_size( &ss ) == 4 );

    rho_ss_remove( &ss, 14 );
    rho_test(
        "when removed an existing item from the sparse set, the set's size has "
        "reduced by one",
        rho_ss_size( &ss ) == 3 );
    rho_test(
        "when removed an existing item from the sparse set, the removed item "
        "is not in the set",
        !rho_ss_has( &ss, 14 ) );
    rho_test(
        "when removed an existing item from the sparse set, all other items "
        "are still in the set",
        rho_ss_has( &ss, 256 ) && rho_ss_has( &ss, 5 )
            && rho_ss_has( &ss, 127 ) );

    for ( rho_ss_id_t id = 0; id <= max_id; ++id ) {
        rho_ss_add( &ss, id );
    }
    rho_test(
        "when added all possible items to the sparse set, its size is correct",
        rho_ss_size( &ss ) == max_id + 1 );
    rho_test(
        "when added all possible items to the sparse set, it is not empty",
        !rho_ss_empty( &ss ) );
    rho_test(
        "when added all possible items to the sparse set, it is full",
        rho_ss_full( &ss ) );

    rho_ss_remove( &ss, 2 );
    rho_test(
        "when removed a single item from the full sparse set, its size is "
        "correct",
        rho_ss_size( &ss ) == max_id );
    rho_test(
        "when removed a single item from the full sparse set, it is not empty",
        !rho_ss_empty( &ss ) );
    rho_test(
        "when removed a single item from the full sparse set, it is full no "
        "more",
        !rho_ss_full( &ss ) );

    rho_ss_free( &ss );
    rho_test(
        "when sparse set is freed, its dense array is deallocated",
        ss.dense == NULL );
    rho_test(
        "when sparse set is freed, its sparse array is deallocated",
        ss.sparse == NULL );
    rho_test(
        "when sparse set is freed, its next item's index is zero",
        ss.n == 0 );
    rho_test( "when sparse set is freed, its capacity is zero", ss.max == 0 );
    rho_test( "when sparse set is freed, it is not full", !rho_ss_full( &ss ) );
    rho_test( "when sparse set is freed, it is empty", rho_ss_empty( &ss ) );

    return 0;
}

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
