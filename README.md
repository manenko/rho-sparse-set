# RhoSparseSet

A simple [STB-style](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt) library that implements a [sparse set](https://manenko.com/2021/05/23/sparse-sets.html) data structure.
The library is written in C.

## Getting started

### Add library to the project

To use the library you have to include it to your project. There are a few options, for example:

- Manually copy `include/rho` directory to the project header directory. This is the simplest one.
- Add the library's repository as a git submodule and configure your build system to look for headers inside `$submodule/include` directory. Make sure you know what are you doing.
- Use CMake and FetchContent. Fetch the library:

  ```cmake
  include(FetchContent)
  FetchContent_Declare(RhoSparseSet
    GIT_REPOSITORY https://gitlab.com/manenko/rho-sparse-set.git
    GIT_TAG        v0.3.0)
  FetchContent_MakeAvailable(RhoSparseSet)
  ```

  Then link your CMake target with the library:

  ```cmake
  target_link_libraries(${PROJECT_NAME} PRIVATE RhoSparseSet)
  ```

### Generate implementation

This is a [STB-style](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt) library. What does it mean? This is a single-header-file library which contains both interface and implementation. To create the implementation, add the following lines to exactly one source file:

```c
#define RHO_SPARSE_SET_IMPLEMENTATION
#include <rho/sparse_set.h>
```

Other source/include files should just `#include <rho/sparse_set.h>`.

### Optional configuration

You can customise the library's behavior by defining the following macros before generating the implementation:

| Macro                 | Description                                           |
| --------------------- | ----------------------------------------------------- |
| `RHO_SS_CUSTOM_ID`    | Define it to provide custom `rho_ss_id_t` type and `rho_ss_id_max` constant. The `(rho_ss_id_max + 1)` should not overflow. If the macro is not defined, the library defines `rho_ss_id_t` as `uint16_t` and `rho_ss_id_max` as `UINT16_MAX - 1`. |
| `RHO_SS_ASSERT(COND)` | Assertion macro. Default: assert(COND).                |
| `RHO_SS_MALLOC(SIZE)` | Memory allocation macro. Default: malloc(SIZE).        |
| `RHO_SS_FREE(PTR)`    | Memory deallocation macro. Default: free(PTR).         |
| `RHO_SS_API_DECL`     | Public function declaration prefix. Default: extern.   |
| `RHO_SS_API_IMPL`     | Public function implementation prefix. Default: empty. |

`RHO_SS_API_IMPL` could be defined as `static` to make implementation private to the source file that creates it.
This way you can have multiple implementations in your project which is useful if you need to have different configurations.

## Example

Remember, you only need to define `RHO_SPARSE_SET_IMPLEMENTATION`, everything else is optional.
The example below configures the library to use cusom `malloc` and `free` functions, as well as `rho_ss_id_t` and `rho_ss_id_max`.

```c
#include <stdint.h>

#define RHO_SS_MALLOC(SIZE) my_mem_alloc(SIZE)
#define RHO_SS_FREE(PTR)    my_mem_dealloc(PTR)

#define RHO_SS_CUSTOM_ID   ( 1 )
typedef uint32_t rho_ss_id_t;
static const rho_ss_id_t rho_ss_id_max = UINT32_MAX - 1;

#define RHO_SPARSE_SET_IMPLEMENTATION
#include <rho/sparse_set.h>
#include <assert.h>

int main( void )
{
    // Create a set that can contain up to 32 items. 
	// Valid items ranged from 0 to 31 included.
    rho_ss_t set = rho_ss_alloc( 31 );
    // The set is empty at the beginning
    assert( rho_ss_empty( &set ) );
    // Adding one item to the set
    rho_ss_add( &set, 5 );
    // Now the set is not empty
    assert( !rho_ss_empty( &set ) );
    // Because it contains one element
    assert( rho_ss_size( &set ) == 1 );
    // And that element is 5
    assert( rho_ss_has( &set, 5 ) );
    // Adding a few more elements
    rho_ss_add( &set, 22 );
    rho_ss_add( &set, 1 );
    rho_ss_add( &set, 16 );
    // The size is now 4
    assert( rho_ss_size( &set ) == 4 );
    // Removing element 22
    rho_ss_remove( &set, 22 );
    // The size is now 3
    assert( rho_ss_size( &set ) == 3 );
    // And element 22 is not in the set anymore
    assert( !rho_ss_has( &set, 22 ) );

    // Print the elements of the set
    for ( rho_ss_id_t i = 0; i < set.n; ++i ) {
        printf( "%d ", set.dense[i] );
    }
    printf("%s", "\n");

    // Clear the set
    rho_ss_clear( &set );
    // Now the set is empty
    assert( rho_ss_empty( &set ) );

    // Finally free up resources used by the set
    rho_ss_free( &set );

    return 0;
}
```

## License

MIT
